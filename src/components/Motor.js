import './motor.css'
export const Motor = (props) => {
    const { assetTag, model, suctionPress, dischargePress, flowRate, impellerTemp, vibration1, vibration2, bearingTemp, current, fla, motorTemp } = props.data
    return (
        <div className="motor">
            <div className="motor__info model_info">
                <div><lable>Asset Tag: </lable>{assetTag}</div> &nbsp; &nbsp;
                <div><lable>Model: </lable>{model}</div>
            </div>
            <div className="motor__image">
                <img src={process.env.PUBLIC_URL + '/motor.svg'} />
            </div>
            <div className="motor__info suction-press">
                Suction Press <br />
                <label className="value">{suctionPress}<sup className="unit">kPa</sup></label>
            </div>
            <div className="motor__info discharge-press">{dischargePress}</div>
            <div className="motor__info flow-rate">{flowRate}</div>
            <div className="motor__info impeller-temp">{impellerTemp}</div>
            <div className="motor__info vibration1">{vibration1}</div>
            <div className="motor__info vibration2">{vibration2}</div>
            <div className="motor__info bearing-temp">{bearingTemp}</div>
            <div className="motor__info current">{current}</div>
            <div className="motor__info fla">{fla}</div>
            <div className="motor__info motor-temp">{motorTemp}</div>
        </div>
    )
}