export const FailureTime = () => {
    return (
        <div className="failure-time">
            <h4>Next failure time</h4>
            <div className="failure-time__data">
                <label>Running Hours</label> <br/>
                <label>Starting Count</label> <br/>
                <label>Time to Failure</label> <br/>
            </div>
        </div>
    )
}