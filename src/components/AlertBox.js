export const AlertBox = (props) => {
    const tableData = props.data
    return (
        <div className="alert-box">
            <h4>Alert &amp; Anomaly</h4>
            <div className="alert-data">
                <table>
                    <thead>
                        <tr>
                            <th>Ack</th>
                            <th>Time</th>
                            <th>Alert/Anomaly</th>
                            <th>Alert Level</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableData.map(td => {
                            return (
                                <tr>
                                    <td>{td.ack}</td>
                                    <td>{td.time}</td>
                                    <td>{td.anomaly}</td>
                                    <td>{td.level}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}