import React from 'react';
import { Chart } from 'react-charts'

export const LineGraph = (props) => {
    console.log(props.data)
    const data = React.useMemo(
        () => [
            {
                label: 'Series 1',
                data: props.data.seriesOne,
            },
            {
                label: 'Series 2',
                data: props.data.seriesTwo,

            }
        ],
        []
    )
    const axes = React.useMemo(
        () => [
            { primary: true, type: 'time', position: 'bottom' },
            { type: 'ordinal', position: 'left' }
        ],
        []
    )
    return (
        <div className="line-graph">
            <Chart data={data} axes={axes} />
        </div>
    )
}