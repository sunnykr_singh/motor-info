export const Recommendation = () => {
    return (
        <div className="recommendation">
            <h4>Recommendation</h4>
            <div className="recommendation__content">
                <h4>Impeller vibration</h4>
                <p>Lorem ipsum dolor sit amet, consectetur <br />
                 adipiscing elit. Fusce a risus nunc. Vestibulum faucibus eros eu nunc blandit, vel rhoncus diam efficitur.</p>
                <h4>Low flowrate</h4>
                <p>Lorem ipsum dolor sit amet, consectetur <br />
                 adipiscing elit. Fusce a risus nunc. Vestibulum faucibus eros eu nunc blandit, vel rhoncus diam efficitur.</p>
            </div>
        </div>
    )
}