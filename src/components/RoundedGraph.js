import React from 'react';
import { buildStyles, CircularProgressbar } from "react-circular-progressbar"
import 'react-circular-progressbar/dist/styles.css';

export const RoundedGraph = (props) => {
    const percentage = props.value * 100
    return (
        <div className="rounded-graph">
            <h4>{props.title}</h4>
            <div className="circular__graph">
                <CircularProgressbar value={props.value} maxValue={1} text={`${percentage}%`}
                    styles={buildStyles({
                        rotation: 0,
                        strokeLinecap: 'butt',
                        textSize: '10px',
                        pathTransitionDuration: 0.5,
                        pathColor: `rgba(0, 255, 112, ${props.value})`,
                        textColor: '#f88',
                        trailColor: '#d6d6d6',
                        backgroundColor: '#3e98c7',
                    })}
                />
            </div>
        </div>
    )
}