import React from 'react'
import { useEffect, useState } from "react";
import { AlertBox } from "./components/AlertBox";
import { FailureTime } from "./components/FailureTime";
import { LineGraph } from "./components/LineGraph";
import { Motor } from "./components/Motor";
import { Recommendation } from "./components/Recommendation";
import { RiskInfo } from "./components/RiskInfo";
import { RoundedGraph } from "./components/RoundedGraph";
import { SideStrip } from "./components/SideStrip";
import { TopStrip } from "./components/TopStrip";

function App() {

  const [data, setData] = useState({
    efficiency: 0.6,
    healthScore: 0.743,
    seriesOne: [[22, 0], [24, 10], [26, 20], [28, 0], [30, 10], [1, 20]],
    seriesTwo: [[22, 0], [24, 10], [26, 20], [28, 0], [30, 10], [1, 20]],
    assetTag: 'P1203', 
    model: 'NP 3400/805', 
    suctionPress: '59.8', 
    dischargePress: 'Discharge Press', 
    flowRate: 'Flowrate', 
    impellerTemp: 'Impeller Temp', 
    vibration1: 'Vibration1', 
    vibration2: 'Vibration2', 
    bearingTemp: 'Bearing Temp', 
    current: 'Current', 
    fla: 'FLA', 
    motorTemp: 'Motor Temp',
    alertData: [
      {
        ack: 1,
        time: '2021-1-14 14:20',
        anomaly: 'sdbsjbd',
        level: 'HIGH'
      },
      {
        ack: 1,
        time: '2021-1-14 14:20',
        anomaly: 'sdbsjbd',
        level: 'HIGH'
      },
      {
        ack: 1,
        time: '2021-1-14 14:20',
        anomaly: 'sdbsjbd',
        level: 'HIGH'
      }]
  });

  useEffect(() => {
    //make api call to get all data in the formate shown above and use setData to update values
  }, [])

  return (
    <div className="app-container">
      <SideStrip />
      <div className="right-container">
        <TopStrip />
        <div className="content-container">
          <div className="content-container__top">
            <Motor data={data}/>
            <div className="round-graph-container">
              <RoundedGraph title={'Efficiency'} value={data.efficiency} />
              <RoundedGraph title={'Health Score'} value={data.healthScore} />
            </div>
            <div className="line-graph-container">
              <LineGraph data={data} />
              <LineGraph data={data} />
              <LineGraph data={data} />
            </div>
          </div>
          <div className="content-container__bottom">
            <FailureTime />
            <Recommendation />
            <RiskInfo />
            <AlertBox data={data.alertData}/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
